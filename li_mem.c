#include<stdio.h>
#include<stdlib.h>
#define KRED "\x1B[31m"
#define KNRM  "\x1B[0m"

void clear_mem_matrix(int rows, int **matrix){
	printf("Antes de liberar memoria con free()\n\tDireccion[ %p ] , Tamano[ %d ]\n",matrix,*matrix);
    for (int i = 0; i < rows; i++)
    {
    	free(matrix[i]);
    }
    free(matrix);
    printf("Despues de liberar memoria con free()\n\tDireccion[ %p ] , Tamano[ %d ]\n",matrix,*matrix);
}

void show_matrix(int rows, int columns, int **matrix){
	printf("%s\t\tCONTENIDO \n",KRED);
	printf("%s",KNRM);
    for(int i=0;i<rows;i++)
    {
    	for (int j = 0; j < columns; j++)
    	{
    		printf(" [%d][%d]%d \n",i,j,matrix[i][j]);
    	}
        
    }
}

void charge_matrix(int rows, int columns, int **matrix){
	for (int i = 0; i < rows; i++)
    {
    	for (int j = 0; j < columns; j++)
    	{
    		matrix[i][j] = rand();
    	}
    }
}

int main()
{
    int **matrix, columns, rows;
    printf("Ingrese el tamano de la matriz->:");
    scanf("%d",&rows);
    columns = rows;
    printf("Antes de asignar con el malloc\n\tDireccion[ %p ] , Tamano[ %d ]\n",matrix,*matrix);
    matrix = (int **) malloc(rows* sizeof(int *));
    printf("Despues de asignar con el malloc\n\tDireccion[ %p ] , Tamano[ %d ]\n",matrix,*matrix);
    for (int i = 0; i < rows; i++)
    {
    	matrix[i] = (int *) malloc(columns* sizeof(int));
    }
    charge_matrix(rows,columns,matrix);
    show_matrix(rows,columns,matrix);
    clear_mem_matrix(rows, matrix);
    return 0;
}